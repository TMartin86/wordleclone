export const constants = {
    MATCH_INDICATOR: "G",
    PARTIAL_MATCH_INDICATOR: "Y",
    NO_MATCH_INDICATOR: "",
    ERROR_WORD_NOT_FOUND: "Word not found. Please try another word.",
    GLOBAL_WORD_LENGTH: 5,
    MAX_ATTEMPTS: 6,
};
