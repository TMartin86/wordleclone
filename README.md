# README #

Vanilla Js Wordle Clone 

Uses their word bank of over 2000 words

### Live Demo

[Demo](https://thomasmartin.dev/)


### How do I get set up? ###

Download repo and then you can locally run the index.html page to launch the app.

You can also use Live Server or similar solutions within VS code or your editor of choice to launch the site.

