import { library } from "./library.js";
import { constants } from "./constants.js";

/* ---- Variables ---- */
const {
    MATCH_INDICATOR,
    PARTIAL_MATCH_INDICATOR,
    NO_MATCH_INDICATOR,
    ERROR_WORD_NOT_FOUND,
    GLOBAL_WORD_LENGTH,
    MAX_ATTEMPTS,
} = constants;

let correctAnswer =
    library.answers[Math.floor(Math.random() * library.answers.length)].split(
        ""
    );
let guessedWords = [];
let attempts = 0;
let guessedLetters = { correct: new Set(), partial: new Set(), no: new Set() };
let userAnswer = [];
let gameOver = false;

/* ---- DOM elements ---- */
const mainGameInput = document.getElementById("main-game-input");
const warning = document.getElementById("warning");
const info = document.getElementById("info");
const reset = document.getElementById("reset-btn");
const remainingGuesses = document.getElementById("remaining-guesses");
const modal = document.getElementById("result-modal");
const modalInfo = document.getElementById("modal-info");
const modalReset = document.getElementById("modal-reset");
const close = document.querySelector(".close");
const noAnswers = document.getElementById("no");

/* ---- Functions ---- */
// generic debounce function used to help give user time to type a full word before checking their input
const debounce = (func, timeout = 350) => {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func(...args);
        }, timeout);
    };
};

// returns a random word from the library
const getNewWord = () => {
    return library.answers[
        Math.floor(Math.random() * library.answers.length)
    ].split("");
};

// returns if the user has won, true or false
const isWinner = () => {
    return (
        guessedLetters.correct.size == GLOBAL_WORD_LENGTH &&
        [...guessedLetters.correct].sort().join("") ==
            [...new Set(correctAnswer)].sort().join("")
    );
};

// checks for a win state, disables main input if game is over
const checkForWin = () => {
    if (isWinner()) {
        mainGameInput.removeEventListener("input", handleUserGuess);
        mainGameInput.disabled = true;
        gameOver = true;
        setTimeout(() => showResultModal(getWinHTML()), 200);
    } else {
        if (attempts === MAX_ATTEMPTS) {
            mainGameInput.disabled = true;
            gameOver = true;
            setTimeout(() => showResultModal(getLossHTML()), 200);
        }
    }
};

// returns winning html string used for updating result modal
const getWinHTML = () => {
    return `<h4 class="won">You got it! Great job!</h4>`;
};

// returns losing html string used for updating result modal
const getLossHTML = () => {
    return `<h4 class="lost">Sorry, you're out of guesses.
    The word was '${correctAnswer.join("")}'.</h4>`;
};

// updates the dom after checking if guesses are matches, partials, or misses
const checkAnswers = () => {
    checkForMatches().forEach((guess, i) => {
        const ele = document.getElementById(`guess-${i}-${attempts}`) || null;
        if (ele) {
            switch (guess) {
                case MATCH_INDICATOR:
                    ele.classList.add("match", "border-match");
                    break;
                case PARTIAL_MATCH_INDICATOR:
                    ele.classList.add("partial-match", "border-partial-match");
                    break;
                default:
                    ele.classList.add("no-match", "border-no-match");
            }
            ele.innerHTML = `<h2 class="guessLetter">${userAnswer[i]}</h2>`;
        }
    });
};

// checks to see if the user answer and correct answer at a given index are a match
const checkAnswer = (i) => {
    if (i < GLOBAL_WORD_LENGTH && i >= 0) {
        if (correctAnswer[i] == userAnswer[i]) {
            guessedLetters.correct.add(userAnswer[i]);
            if (guessedLetters.partial.has(userAnswer[i]))
                guessedLetters.partial.delete(userAnswer[i]);
            return MATCH_INDICATOR;
        } else if (correctAnswer.includes(userAnswer[i])) {
            if (!guessedLetters.correct.has(userAnswer[i]))
                guessedLetters.partial.add(userAnswer[i]);
            return PARTIAL_MATCH_INDICATOR;
        } else {
            guessedLetters.no.add(userAnswer[i]);
            return NO_MATCH_INDICATOR;
        }
    } else {
        return NO_MATCH_INDICATOR;
    }
};

// checks for answers and returns an array of results
const checkForMatches = () => {
    return [
        checkAnswer(0),
        checkAnswer(1),
        checkAnswer(2),
        checkAnswer(3),
        checkAnswer(4),
    ];
};

// checks the libary contains the word
const checkWordExists = (word = "") => {
    return (
        word.toLowerCase() &&
        (library.answers.includes(word.toLowerCase()) ||
            library.guesses.includes(word.toLowerCase()))
    );
};

// updates dom to reflect missed letters
const updateDomAnswers = () => {
    resetDomAnswers();
    [...guessedLetters.no].sort().forEach((letter) => {
        noAnswers.innerHTML += `<span class="strike">${letter}</span>`;
    });
};

// updates the dom to reflect number of guesses
const updateRemainingGuesses = () => {
    remainingGuesses.innerHTML = `<h4>${
        MAX_ATTEMPTS - (MAX_ATTEMPTS - attempts)
    } / ${MAX_ATTEMPTS}</h4>`;
};

// updates the dom to show a warning message
const showWarning = (message = "") => {
    warning.innerText = message;
    warning.style.display = "block";
    info.style.display = "none";
};

// updates the dom to hide a warning message
const hideWarning = () => {
    warning.innerText = "";
    warning.style.display = "none";
    info.style.display = "block";
};

// updates the dom to show the end game result modal
const showResultModal = (htmlString = "") => {
    modal.style.display = "block";
    if (htmlString) {
        modalInfo.innerHTML = "";
        modalInfo.innerHTML = htmlString;
    }
};

// updates the dom to hide the end game result modal
const hideResultModal = () => {
    modalInfo.innerHTML = "";
    modal.style.display = "none";
};

// updates the dom to show the group of letters which are misses
const showBadLetters = () => {
    noAnswers.style.display = "flex";
};

// updates the dom to hide the group of letters which are misses
const hideBadLetters = () => {
    noAnswers.style.display = "none";
};

// updates the dom to set focus on main game input
const focusMainGameInput = () => {
    mainGameInput.focus();
};

// increases the total attempts number
const increaseAttempts = () => {
    if (attempts < MAX_ATTEMPTS) ++attempts;
};

// debounce checking the user's answer so that they have time to type
const checkUserAnswer = debounce((word) => {
    guessedWords.push(userAnswer);
    userAnswer = word.split("");
    handleUserAnswer();
}, 550);

// handles checking a users answer and looking for a win
const handleUserAnswer = () => {
    checkAnswers();
    increaseAttempts();
    checkForWin();
    updateRemainingGuesses();
    updateDomAnswers();
    showBadLetters();
    if (!gameOver) resetMainGameInput();
};

// resets all variabels to their default state
const resetVariables = () => {
    guessedLetters = { correct: new Set(), partial: new Set(), no: new Set() };
    guessedWords = [];
    userAnswer = [];
    attempts = 0;
};

// updates the dom to clear the missed answers
const resetDomAnswers = () => {
    noAnswers.innerHTML = "";
};

// updates the correct answer to be a new word
const resetCorrectAnswer = () => {
    correctAnswer = getNewWord();
};

// clears, enables, and focuses the main game input
const resetMainGameInput = () => {
    mainGameInput.value = "";
    mainGameInput.disabled = false;
    focusMainGameInput();
};

// updates the dom to clear all guessed letters by removing cooresponding classes
const resetBoard = () => {
    document.querySelectorAll(".guess").forEach((guess) => {
        guess.classList.remove(
            "no-match",
            "partial-match",
            "match",
            "borderless",
            "border-match",
            "border-no-match",
            "border-partial-match"
        );
        guess.innerHTML = "";
    });
};

// handles resetting a game to a new game state
const resetGame = () => {
    resetVariables();
    resetCorrectAnswer();
    resetMainGameInput();
    initGameInfo();
    resetBoard();
    resetDomAnswers();
    hideResultModal();
    initEventListeners();
};

// handles a user inputing a word
const handleUserGuess = (e) => {
    hideWarning();
    const value = e.target.value;
    if (value.length === GLOBAL_WORD_LENGTH) {
        e.stopPropagation();
        e.preventDefault();
        if (checkWordExists(value)) {
            checkUserAnswer(value);
        } else {
            showWarning(ERROR_WORD_NOT_FOUND);
        }
    }
};

// initializes event listeners for dom elements
const initEventListeners = () => {
    mainGameInput.addEventListener("input", handleUserGuess);
    reset.addEventListener("click", resetGame);
    modalReset.addEventListener("click", resetGame);
    close.addEventListener("click", hideResultModal);
};

// initializes game state visuals
const initGameInfo = () => {
    gameOver = false;
    info.innerText = "Guess a 5 letter word";
    updateRemainingGuesses();
    hideWarning();
    hideBadLetters();
};

// prints debug stats
const printDebugStats = (msg = "", item = "") => {
    const groupMsg = msg ? `Debug-Stats: ${msg}` : `Debug-Stats`;
    console.group(groupMsg);
    printGameState();
    printDebugItems();
    console.groupEnd();
};

// prints the state of the game
const printGameState = () => {
    console.group("Game State");
    console.log("guessesLetters: ", guessedLetters);
    console.log("correctAnswer: ", correctAnswer);
    console.log("userAnswer: ", userAnswer);
    console.log("attempts: ", attempts);
    console.groupEnd();
};

// print additional items passed to printDebugStats
const printDebugItems = (item = "") => {
    if (item) {
        console.group("Passed in Args");
        if (typeof item === "object" && item !== null) {
            Object.entries(item).forEach((key, value) => {
                console.log(key, value);
            });
        } else if (Array.isArray(item)) {
            item.forEach((item) => {
                console.log(item);
            });
        } else {
            console.log(item);
        }
        console.groupEnd();
    }
};

// main game
const main = () => {
    hideResultModal();
    initGameInfo();
    focusMainGameInput();
    initEventListeners();
};

/* ---- run app ---- */
main();
